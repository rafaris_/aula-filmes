package com.itau.filmes;

public class Locadora {
	public static Filme[] obterFilmes() {
	Ator ator1 = new Ator();
	ator1.nome = "Keanu";
	ator1.sobrenome = "Reeves";
	
	Ator ator2 = new Ator();
	ator2.nome = "Carrie-Ane";
	ator2.sobrenome = "Moss";
	
	Ator ator3 = new Ator();
	ator3.nome = "Carrie-Ane";
	ator3.sobrenome = "Moss";
	Filme filme1 = new Filme();
	filme1.titulo = "Matrix";
	filme1.anoLancamento = 1999;
	filme1.genero = Genero.FICCAO;

	filme1.atores = new Ator[2];
	filme1.atores[0] = ator1;
	filme1.atores[1] = ator2;
	Filme[] filmes = new Filme[1];
	filmes[0] = filme1;
	return filmes;	
	}		
}
