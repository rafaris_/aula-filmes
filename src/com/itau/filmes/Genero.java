package com.itau.filmes;

public enum Genero {
	COMEDIA("Comédia"),
	ACAO("Ação"),
	DRAMA("Drama"),
	TERROR("Terror"),
	FICCAO("Ficção");
	
	private String descricao;
	
	private Genero(String descricao) {
			this.descricao = descricao;
	}
	public String toString() {
		return descricao;
	}
}
